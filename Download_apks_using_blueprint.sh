#! /bin/sh
sudo apt update 
sudo apt install axel -y 
rm -rf  $PWD/../Download_Prebuilt/
mkdir  -p $PWD/../Download_Prebuilt/Simple_Gallery && axel -o $PWD/../Download_Prebuilt/Simple_Gallery/simplegallery.apk 'https://sourceforge.net/projects/linux-stuff/files/Android_Apps/Simple_GalleryPro_393.apk'
mkdir  -p $PWD/../Download_Prebuilt/ViaBrowser       && axel  -o $PWD/../Download_Prebuilt/ViaBrowser/via.apk 'https://sourceforge.net/projects/linux-stuff/files/Android_Apps/via-release.apk'
mkdir  -p $PWD/../Download_Prebuilt/MX_Player        && axel -o $PWD/../Download_Prebuilt/MX_Player/mxplayer.apk 'https://sourceforge.net/projects/linux-stuff/files/Android_Apps/MX-Player-v1.68.2-Premium.apk'
mkdir  -p $PWD/../Download_Prebuilt/filemanagerplus  && axel -o $PWD/../Download_Prebuilt/filemanagerplus/filemanagerplus.apk 'https://sourceforge.net/projects/linux-stuff/files/Android_Apps/file_manager_plus%5B3.1.8%5D.apk'
####################################################################################################################################################################################################################################

echo '

 android_app_import {
	name: "filemanagerplus",
	apk: "filemanagerplus/filemanagerplus.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
         overrides: ["FilesPrebuilt" , "DocumentsUI"],
      }

 android_app_import {
	name: "mxplayer",
	apk: "MX_Player/mxplayer.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
      }

android_app_import {
	name: "via",
	apk: "ViaBrowser/via.apk",
	presigned: true,
	dex_preopt: {
		enabled: false,
	},
	privileged: true,
	product_specific: true,
	overrides: ["Browser2" , "Jelly"],
      }



android_app_import {
        name: "simplegallery",
        apk: "Simple_Gallery/simplegallery.apk",
        presigned: true,
        dex_preopt: {
                enabled: false,
        },
        privileged: true,
        product_specific: true,
        overrides: ["Gallery2"],
} '  >>  $PWD/../Download_Prebuilt/Android.bp



echo '
PRODUCT_PACKAGES += \
simplegallery \
via \
mxplayer \
filemanagerplus' >>   $PWD/../device/xiaomi/redwood/device.mk
