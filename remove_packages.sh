
#! /bin/sh
mkdir -p $PWD/../device/xiaomi/redwood/RemovePackages
echo '
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := RemovePackages
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_OVERRIDES_PACKAGES :=  AdaptiveVPNPrebuilt AmbientSensePrebuilt AmbientStreaming AndroidAutoStubPrebuilt AppDirectedSMSService AudioFX CarrierMetrics ConnMO DCMO DevicePolicyPrebuilt DiagnosticsToolPrebuilt DMService Drive Duo Flipendo FMRadio GCS GoogleCamera GoogleFeedback HealthConnectPrebuilt Jelly MaestroPrebuilt Matlog MicropaperPrebuilt Music MusicFX MyVerizonServices NgaResources OBDM_Permissions obdm_stub oemDmTrigger OPScreenRecord Ornament Papers Photos PhotoTable PixelLiveWallpaperPrebuilt PixelWallpapers2020 PixelWallpapers2021 PixelWallpapers2022 PixelWallpapers2022a PixelWallpapers2023Tablet RecorderPrebuilt SafetyHubPrebuilt SCONE ScribePrebuilt SecurityHubPrebuilt Showcase Snap SoundAmplifierPrebuilt SprintDM SprintHM talkback TipsPrebuilt TurboPrebuilt Tycho USCCDM Velvet Video Videos VZWAPNLib VzwOmaTrigger YouTube YouTubeMusicPrebuilt YTMusic  

LOCAL_UNINSTALLABLE_MODULE := true
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_SRC_FILES := /dev/null
include $(BUILD_PREBUILT) ' >>	  $PWD/../device/xiaomi/redwood/RemovePackages/Android.mk


echo '
PRODUCT_PACKAGES += \
RemovePackages' >> $PWD/../device/xiaomi/redwood/device.mk
